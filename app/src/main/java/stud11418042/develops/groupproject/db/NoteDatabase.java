package stud11418042.develops.groupproject.db;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import stud11418042.develops.groupproject.dao.DaoAccess;
import stud11418042.develops.groupproject.model.Note;

@Database(entities = {Note.class}, version = 1, exportSchema = false)
public abstract class NoteDatabase extends RoomDatabase {

    public abstract DaoAccess daoAccess();
}
